0 INITIALIZE_TRIM IP(IP5) BEAM(BEAM1) PLANE(SEPARATION) UNITS(SIGMA)
1 SECONDS_WAIT 46.0
2 RELATIVE_TRIM IP5 BEAM1 SEPARATION -4.5 SIGMA
3 START_FIT SEPARATION GAUSSIAN_PLUS_CONSTANT
4 SECONDS_WAIT 116.0
5 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
6 SECONDS_WAIT 81.0
7 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
8 SECONDS_WAIT 57.0
9 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
10 SECONDS_WAIT 41.0
11 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
12 SECONDS_WAIT 32.0
13 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
14 SECONDS_WAIT 27.0
15 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
16 SECONDS_WAIT 24.0
17 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
18 SECONDS_WAIT 23.0
19 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
20 SECONDS_WAIT 23.0
21 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
22 SECONDS_WAIT 23.0
23 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
24 SECONDS_WAIT 23.0
25 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
26 SECONDS_WAIT 23.0
27 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
28 SECONDS_WAIT 24.0
29 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
30 SECONDS_WAIT 27.0
31 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
32 SECONDS_WAIT 32.0
33 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
34 SECONDS_WAIT 41.0
35 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
36 SECONDS_WAIT 57.0
37 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
38 SECONDS_WAIT 81.0
39 RELATIVE_TRIM IP5 BEAM1 SEPARATION 0.5 SIGMA
40 SECONDS_WAIT 116.0
41 END_FIT
42 RELATIVE_TRIM IP5 BEAM1 SEPARATION -4.5 SIGMA
43 END_SEQUENCE
