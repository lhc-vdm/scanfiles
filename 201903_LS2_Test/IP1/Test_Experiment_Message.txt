0 INITIALIZE_TRIM IP(IP1) BEAM(BEAM1,BEAM2) PLANE(SEPARATION) UNITS(SIGMA)
1 SECONDS_WAIT 20.0
2 MESSAGE Checkpoint-1
3 RELATIVE_TRIM IP1 BEAM1 SEPARATION 0.5 SIGMA IP1 BEAM2 SEPARATION -0.5 SIGMA
4 SECONDS_WAIT 10.0
5 MESSAGE Checkpoint-2
6 SECONDS_WAIT 30.0
7 END_SEQUENCE
