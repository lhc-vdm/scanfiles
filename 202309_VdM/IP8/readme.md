1. vdM_XY_3.5_sigma.txt - +/-3.5 sigma X-Y vdM scan

2. vdM_2D_2.45_sigma_in_xy_3.46_in_r.txt - 2D spiral+diagonal vdM scan,

then, repeat 1+2:

3. vdM_XY_3.5_sigma.txt - +/-3.5 sigma X-Y vdM scan

4. vdM_2D_2.45_sigma_in_xy_3.46_in_r.txt - 2D spiral+diagonal vdM scan,

Then, inject SMOG and immediately afterward make LSC twice (this will give enough time for pressure stabilization for next vdMs):

5. LSC_4.5_sigma.txt - "leap-frog" LSC,

6. LSC_4.5_sigma.txt - "leap-frog" LSC,

Then:

7. vdM_XY_4.5_sigma.txt - +/-4.5 sigma X-Y vdM

8. vdM_2D_2.45_sigma_in_xy_3.46_in_r.txt - the same 2D spiral+diagonal,

9. vdM_XY_4.5_sigma_more_steps.txt - +/-4.5 sigma X-Y vdM with finer steps in the center


The time estimate is:

no-SMOG, 1-2 twice, ie. XY+2D twice: (30 + 36.8) + (30 + 36.8) = 133.6 min = 2h 14 min       

SMOG  2xLSC + wide XY + 2D + wide XY w/more steps: 30.8*2 + 26.5 + 36.8 + 30.8 = 157.7 min = 2h 38 min

Total: 4h 51 min, plus time for communications and changing sequences.
