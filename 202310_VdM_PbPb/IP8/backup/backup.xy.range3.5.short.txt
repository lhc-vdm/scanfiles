0 INITIALIZE_TRIM IP(IP8) BEAM(BEAM1,BEAM2) PLANE(SEPARATION,CROSSING) UNITS(SIGMA)
1 SECONDS_WAIT 30
2 RELATIVE_TRIM IP8 BEAM1 CROSSING -3.5 SIGMA IP8 BEAM2 CROSSING 3.5 SIGMA
3 SECONDS_WAIT 15
4 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
5 SECONDS_WAIT 15
6 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
7 SECONDS_WAIT 15
8 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
9 SECONDS_WAIT 15
10 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
11 SECONDS_WAIT 15
12 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
13 SECONDS_WAIT 15
14 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
15 SECONDS_WAIT 15
16 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
17 SECONDS_WAIT 30
18 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
19 SECONDS_WAIT 15
20 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
21 SECONDS_WAIT 15
22 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
23 SECONDS_WAIT 15
24 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.35 SIGMA IP8 BEAM2 CROSSING -0.35 SIGMA
25 SECONDS_WAIT 15
26 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
27 SECONDS_WAIT 15
28 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
29 SECONDS_WAIT 15
30 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.7 SIGMA IP8 BEAM2 CROSSING -0.7 SIGMA
31 SECONDS_WAIT 15
32 RELATIVE_TRIM IP8 BEAM1 CROSSING -3.5 SIGMA IP8 BEAM2 CROSSING 3.5 SIGMA
33 SECONDS_WAIT 30
34 SECONDS_WAIT 30
35 RELATIVE_TRIM IP8 BEAM1 SEPARATION -3.5 SIGMA IP8 BEAM2 SEPARATION 3.5 SIGMA
36 SECONDS_WAIT 15
37 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
38 SECONDS_WAIT 15
39 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
40 SECONDS_WAIT 15
41 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
42 SECONDS_WAIT 15
43 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
44 SECONDS_WAIT 15
45 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
46 SECONDS_WAIT 15
47 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
48 SECONDS_WAIT 15
49 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
50 SECONDS_WAIT 30
51 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
52 SECONDS_WAIT 15
53 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
54 SECONDS_WAIT 15
55 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
56 SECONDS_WAIT 15
57 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.35 SIGMA IP8 BEAM2 SEPARATION -0.35 SIGMA
58 SECONDS_WAIT 15
59 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
60 SECONDS_WAIT 15
61 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
62 SECONDS_WAIT 15
63 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.7 SIGMA IP8 BEAM2 SEPARATION -0.7 SIGMA
64 SECONDS_WAIT 15
65 RELATIVE_TRIM IP8 BEAM1 SEPARATION -3.5 SIGMA IP8 BEAM2 SEPARATION 3.5 SIGMA
66 SECONDS_WAIT 30
67 END_SEQUENCE
