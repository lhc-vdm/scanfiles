https://lhcvdm.web.cern.ch/lhcvdm/


It is possible to use magnet standardization in front of diagonal scans as well. 
In this case, `STD_seq_B1upB2do.txt` has to be run before the scan if the angle is positive, and `STD_seq_negativeAngle.txt` if the angle is negative. 

For vdM scans running `STD_seq_B1upB2do.txt` once before the scan pair is sufficient unless there is an optimization before the second member of the scan pair. Optimizations for sure negate the effect of the magnet standardization.  

## vdM block:
- SupS_5sig_5sig_300s.txt

- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_x_25st_30s.txt
- *Beam optimization*
- *STD_seq_B1upB2do.txt*
- VDM_y_25st_30s.txt

- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_+45deg_25st_30s.txt
- *Beam optimization*
- STD_seq_negativeAngle.txt
- VDM_-45deg_25st_30s.txt

- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_y_25st_30s.txt
- *Beam optimization*
- *STD_seq_B1upB2do.txt*
- VDM_x_25st_30s.txt

- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_+30deg_25st_30s.txt (or VDM_+30deg_17st_30s.txt)
- *Beam optimization*
- STD_seq_B1upB2do.txt 
- VDM_+60deg_25st_30s.txt (or VDM_+60deg_17st_30s.txt) 
- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_y_25st_30s.txt  
- Beam optimization
- STD_seq_negativeAngle.txt
- VDM_-60deg_25st_30s.txt (or VDM_-60deg_17st_30s.txt) 
- *Beam optimization*
- STD_seq_negativeAngle.txt
- VDM_-30deg_25st_30s.txt (or VDM_-30deg_17st_30s.txt)

- Beam optimization
- STD_seq_B1upB2do.txt
- VDM_x_25st_30s.txt
- *Beam optimization*
- *STD_seq_B1upB2do.txt*
- VDM_y_25st_30s.txt

- SupS_5sig_5sig_300s.txt

## LSC block:
- *STD_seq_B1upB2up.txt*
- LSC_x_2x9st_30s_moveSep.txt (or LSC_x_2x5st_30s.txt)
- LSC_y_2x9st_30s_moveSep.txt (or LSC_y_2x5st_30s.txt)
- *STD_seq_B1upB2up.txt*
- LSC_45deg_2x9st_30s_moveSep.txt
- *STD_seq_B1upB2up.txt*
- HYST_x_25st_r6_30s.txt
- HYST_y_25st_r6_30s.txt
- LSC_both_3x2st_r9_30s.txt
- ~~LSC_x_3x2st_r9_30s.txt~~ (time is up)
- ~~LSC_x_3x2st_r5_30s.txt~~ 
- ~~LSC_y_3x2st_r9_30s.txt~~
- ~~LSC_y_3x2st_r5_30s.txt~~

**OR**
- *STD_seq_B1upB2up.txt*
- LSC_x_2x9st_30s_moveSep.txt (or LSC_x_2x5st_30s.txt)
- LSC_y_2x9st_30s_moveSep.txt (or LSC_y_2x5st_30s.txt)
- *STD_seq_B1upB2up.txt*
- LSV_B1X_5x3st_r7_30s.txt
- LSV_B1Y_5x3st_r7_30s.txt
- *STD_seq_B1upB2up.txt*
- LSV_B2X_5x3st_r7_30s.txt
- LSV_B2Y_5x3st_r7_30s.txt
