The proposed sequence is the following, first without SMOG:

1) vdM_XY_3.5_sigma.txt -  +/-3.5 sigma X-Y vdM scan

2) vdM_2D_2.45_sigma_in_xy_3.46_in_r.txt - 2D spiral+diagonal vdM scan,

- Optionally: fully close / open / close / open Velo to +/-0.5 mm with
stationary beams - this might be dropped, to be seen with Velo experts
(for a calibration transfer from open to close if we'll run with Velo
closed in physics)

- Inject SMOG

3) vdM_XY_4.5_sigma.txt -  +/-4.5 sigma X-Y vdM

4) vdM_2D_2.45_sigma_in_xy_3.46_in_r.txt - the same 2D spiral+diagonal,

5) LSC_4.5_sigma.txt - "leap-frog" LSC,

6) vdM_XY_4.5_sigma_more_steps.txt -  +/-4.5 sigma X-Y vdM with finer
steps in the center
