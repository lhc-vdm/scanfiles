Proposed program:

**Between each scan run optimization**
Standardization incorporated in each scanfile

0. Possible fast scan while Atlas commisions trigger etc (30' window max)
202410_VdM_pp_reference/IP5/VDM_xy_25st_17s.txt (1176s)

1. 202410_VdM_pp_reference/IP5/SupS_5sig_5sig_300s.txt.  (351s)
2. 202410_VdM_pp_reference/IP5/VDM_xy_25st_30s.txt.      (1827s)
3. 202410_VdM_pp_reference/IP5/VDM_+-45deg_25st_30s.txt. (1908s)
4. 202410_VdM_pp_reference/IP5/VDM_yx_25st_30s.txt..     (1827s)
5. 202410_VdM_pp_reference/IP5/VDM_+-45deg_25st_30s.txt. (1908s)
6. 202410_VdM_pp_reference/IP5/VDM_xy_25st_30s.txt..     (1827s)
7. 202410_VdM_pp_reference/IP5/SupS_5sig_5sig_300s.txt.  (351s)


**In a separate fill**

0. 202410_VdM_pp_reference/IP5/MSTD_length_scale.txt.    (94s)
1. 202410_VdM_pp_reference/IP5/LSC_x_2x5st_30s.txt.      (564s)
2. 202410_VdM_pp_reference/IP5/LSC_y_2x5st_30s.txt.      (564s)

**Reserve**

1. 202410_VdM_pp_reference/IP5/OS_x_2p25sig_17st_30s.txt.   (713s)
2. 202410_VdM_pp_reference/IP5/OS_y_2p25sig_17st_30s.txt.   (713s)
3. 202410_VdM_pp_reference/IP5/OS_xy_2sig_23st_30s_GP.txt.  (1815s) (incorporating standardisation)
4. 202410_VdM_pp_reference/IP5/OS_xy_2sig_11st_30s_GP.txt   (1093s). (In case useful and faster)