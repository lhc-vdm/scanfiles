0 INITIALIZE_TRIM IP(IP1) BEAM(BEAM1,BEAM2) PLANE(SEPARATION,CROSSING) UNITS(SIGMA)
1 SECONDS_WAIT 15
2 SECONDS_WAIT 60
3 RELATIVE_TRIM IP1 BEAM1 SEPARATION -3.00 SIGMA IP1 BEAM2 SEPARATION 3.00 SIGMA
4 SECONDS_WAIT 60
5 RELATIVE_TRIM IP1 BEAM1 SEPARATION 3.00 SIGMA IP1 BEAM2 SEPARATION -3.00 SIGMA
6 SECONDS_WAIT 60
7 RELATIVE_TRIM IP1 BEAM1 SEPARATION 3.00 SIGMA IP1 BEAM2 SEPARATION -3.00 SIGMA
8 SECONDS_WAIT 60
9 RELATIVE_TRIM IP1 BEAM1 SEPARATION -3.00 SIGMA IP1 BEAM2 SEPARATION 3.00 SIGMA
10 SECONDS_WAIT 60
11 RELATIVE_TRIM IP1 BEAM1 CROSSING -3.00 SIGMA IP1 BEAM2 CROSSING 3.00 SIGMA
12 SECONDS_WAIT 60
13 RELATIVE_TRIM IP1 BEAM1 CROSSING 3.00 SIGMA IP1 BEAM2 CROSSING -3.00 SIGMA
14 SECONDS_WAIT 60
15 RELATIVE_TRIM IP1 BEAM1 CROSSING 3.00 SIGMA IP1 BEAM2 CROSSING -3.00 SIGMA
16 SECONDS_WAIT 60
17 RELATIVE_TRIM IP1 BEAM1 CROSSING -3.00 SIGMA IP1 BEAM2 CROSSING 3.00 SIGMA
18 SECONDS_WAIT 60
19 END_SEQUENCE
