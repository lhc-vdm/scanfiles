0 INITIALIZE_TRIM IP(IP8) BEAM(BEAM1,BEAM2) PLANE(SEPARATION,CROSSING) UNITS(SIGMA)
1 SECONDS_WAIT 10
2 RELATIVE_TRIM IP8 BEAM1 CROSSING -3.1 SIGMA IP8 BEAM2 CROSSING -3.1 SIGMA
3 SECONDS_WAIT 20
4 RELATIVE_TRIM IP8 BEAM1 CROSSING 1.55 SIGMA IP8 BEAM2 CROSSING 1.55 SIGMA
5 SECONDS_WAIT 20
6 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.775 SIGMA IP8 BEAM2 CROSSING 0.775 SIGMA
7 SECONDS_WAIT 20
8 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.775 SIGMA IP8 BEAM2 CROSSING 0.775 SIGMA
9 SECONDS_WAIT 10
10 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.775 SIGMA IP8 BEAM2 CROSSING 0.775 SIGMA
11 SECONDS_WAIT 20
12 RELATIVE_TRIM IP8 BEAM1 CROSSING 0.775 SIGMA IP8 BEAM2 CROSSING 0.775 SIGMA
13 SECONDS_WAIT 20
14 RELATIVE_TRIM IP8 BEAM1 CROSSING 1.55 SIGMA IP8 BEAM2 CROSSING 1.55 SIGMA
15 SECONDS_WAIT 20
16 RELATIVE_TRIM IP8 BEAM1 CROSSING -3.1 SIGMA IP8 BEAM2 CROSSING -3.1 SIGMA
17 SECONDS_WAIT 10
18 SECONDS_WAIT 10
19 RELATIVE_TRIM IP8 BEAM1 SEPARATION -3.1 SIGMA IP8 BEAM2 SEPARATION -3.1 SIGMA
20 SECONDS_WAIT 20
21 RELATIVE_TRIM IP8 BEAM1 SEPARATION 1.55 SIGMA IP8 BEAM2 SEPARATION 1.55 SIGMA
22 SECONDS_WAIT 20
23 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.775 SIGMA IP8 BEAM2 SEPARATION 0.775 SIGMA
24 SECONDS_WAIT 20
25 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.775 SIGMA IP8 BEAM2 SEPARATION 0.775 SIGMA
26 SECONDS_WAIT 10
27 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.775 SIGMA IP8 BEAM2 SEPARATION 0.775 SIGMA
28 SECONDS_WAIT 20
29 RELATIVE_TRIM IP8 BEAM1 SEPARATION 0.775 SIGMA IP8 BEAM2 SEPARATION 0.775 SIGMA
30 SECONDS_WAIT 20
31 RELATIVE_TRIM IP8 BEAM1 SEPARATION 1.55 SIGMA IP8 BEAM2 SEPARATION 1.55 SIGMA
32 SECONDS_WAIT 20
33 RELATIVE_TRIM IP8 BEAM1 SEPARATION -3.1 SIGMA IP8 BEAM2 SEPARATION -3.1 SIGMA
34 SECONDS_WAIT 10
35 END_SEQUENCE
